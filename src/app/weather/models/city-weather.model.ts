import { WeatherDetail } from './weather-detail.model';


export interface City {
  id: number;
  name: string;
  coord: any;
  country: any,
  population: number;
  timezone: number;
  sunrise: number;
  sunset: number;
}

export interface Weather {
  temp: number;
  feels_like: number;
  pressure: number;
  humidity: number;
}


export interface CityWeather {
  coord: any;
  weather: WeatherDetail[];
  base: string;
  main: Weather;
  visibility: number;
  wind: {
    speed: number;
    deg: number;
    gust: number;
  };
  clouds: {
    all: number;
  };
  rain?: {
    '1h': number;
    '3h': number;
  };
  snow?: {
    '1h': number;
    '3h': number;
  };
  dt: number;
  sys: {
    type: number;
    id: number;
    message: string;
    country: any;
    sunrise: number;
    sunset: number;
  };
  timezone: number;
  id: number;
  name: string;
  cod: number;
}
