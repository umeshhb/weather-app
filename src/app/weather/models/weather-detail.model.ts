export interface WeatherDetail {
  id: number;
  main: string;
  description: string;
  icon: string;
}
