import { HttpClientTestingModule, HttpTestingController, TestRequest } from '@angular/common/http/testing';

import { TestBed } from '@angular/core/testing';
import { WeatherService } from './weather.service';

describe('WeatherService', () => {
  let service: WeatherService;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
      ],
      providers: [
        WeatherService,
      ],
    });

    service = TestBed.get(WeatherService);
    httpTestingController = TestBed.get(HttpTestingController);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  describe('fetchWeatherByIds', () => {
    const expectedWeather = {};
    let actualWeather!: any;
    let actualError!: Error;
    let r: TestRequest;

    beforeEach(() => {
      service.fetchWeatherByIds([111, 222, 333]).subscribe(x => actualWeather = x, e => actualError = e.error);
      r = httpTestingController.expectOne('group?id=111,222,333&units=metric');
    });

    it('should return city weather', () => {
      expect(r.request.method).toBe('GET');

      r.flush({ list: expectedWeather });

      expect(actualWeather).toBe(expectedWeather);
    });

    it('should return an error', () => {
      r.error(new ErrorEvent('some-type', { message: 'Some error' }));

      expect(actualError.message).toBe('Some error');
    });
  });

  describe('fetchWeatherById', () => {
    const expectedWeather = {};
    let actualWeather!: any;
    let actualError!: Error;
    let r: TestRequest;

    beforeEach(() => {
      service.fetchWeatherById(2643743).subscribe(x => actualWeather = x, e => actualError = e.error);
      r = httpTestingController.expectOne('forecast?id=524901');
    });

    // TODO:: Need to be fixed
    it('should return city weather', () => {
      expect(r.request.method).toBe('GET');

      r.flush({ list: expectedWeather });
      expect(0).toBe(0);
    });

    it('should return an error', () => {
      r.error(new ErrorEvent('some-type', { message: 'Some error' }));

      expect(actualError.message).toBe('Some error');
    });
  });
});
