import { HttpClient, HttpParams } from '@angular/common/http';
import { map, filter, pluck } from 'rxjs/operators';

import { CityWeather } from '../models/city-weather.model';
import { ForecastWeather } from '../models/forecast-weather.model';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class WeatherService {
  constructor(private readonly http: HttpClient) {}

  private baseURL:string = 'https://api.openweathermap.org/data/2.5';

  fetchCityId(name: string): Observable<number> {
    const params = { units: 'metric', q: name };
    return this.http.get<CityWeather>(`${this.baseURL}/weather`, { params }).pipe(
      pluck('id'),
    );
  }

  fetchWeatherById(id: number): Observable<any | Error> {
    const params = { id: id.toString() };
    const list = this.http.get<{ list: ForecastWeather[] }>(`${this.baseURL}/forecast`, { params }).pipe(
      pluck('list'),
      map(data => {
        return data && data.filter((item: ForecastWeather) => item.dt_txt.indexOf('09:00') > 0 );
      })
    );
    return list;
  }

  fetchWeatherByIds(ids: number[]): Observable<CityWeather[] | Error> {
    const params = { id: ids.join(','), units: 'metric' };
    return this.http.get<{ list: CityWeather[] }>(`${this.baseURL}/group`, { params }).pipe(
      pluck('list'),
    );
  }

}
