import { CityWeatherComponent } from './components/city-weather-item/city-weather.component';
import { CityWeatherPageComponent } from './components/city-weather-page/city-weather-page.component';
import { ForecastComponent } from './components/forecast/forecast.component';
import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { WeatherRoutingModule } from './weather-routing.module';

@NgModule({
  imports: [
    SharedModule,
    WeatherRoutingModule,
  ],
  declarations: [
    CityWeatherComponent,
    CityWeatherPageComponent,
    ForecastComponent,
  ],
  exports: [
    CityWeatherComponent,
  ],
})
export class WeatherModule {}
