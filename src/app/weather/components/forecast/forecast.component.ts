import { Component, Input } from '@angular/core';
import { ForecastWeather } from '../../models/forecast-weather.model';

@Component({
  selector: 'app-forecast',
  templateUrl: './forecast.component.html',
  styleUrls: ['./forecast.component.css']
})
export class ForecastComponent{
  @Input()
  weather!: ForecastWeather;

  size: '4x' | '2x' | undefined;


  get weatherIconURL(): string {
    const icon = [this.weather.weather[0].icon];

    if (this.size) {
      icon.push(this.size);
    }

    console.log(icon);

    return `http://openweathermap.org/img/wn/${icon}@2x.png`;
  }

}
