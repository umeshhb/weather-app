import { Component, Input } from '@angular/core';

import { CityWeather } from '../../models/city-weather.model';

@Component({
  selector: 'app-city-weather',
  templateUrl: 'city-weather.component.html',
  styleUrls: ['city-weather.component.css'],
})
export class CityWeatherComponent {
  @Input()
  weather!: CityWeather;

  size: '2x' | '4x' | undefined;


  get weatherIconURL(): string {
    const icon = [this.weather.weather[0].icon];

    if (this.size) {
      icon.push(this.size);
    }

    return `http://openweathermap.org/img/wn/${icon.join('@')}.png`;
  }

}
