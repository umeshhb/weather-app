import { Component, Input, NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CityWeather } from '../../models/city-weather.model';
import { CityWeatherComponent } from './city-weather.component';

describe('CityWeatherComponent', () => {
  let fixture: ComponentFixture<CityWeatherComponent>;

  beforeEach(() => {
    fixture = TestBed.configureTestingModule({
      declarations: [
        CityWeatherComponent
      ],
      schemas: [
        NO_ERRORS_SCHEMA,
      ],
    }).createComponent(CityWeatherComponent);

    fixture.componentInstance.weather = {
      name: 'City',
      main: {
        temp: 30.55,
      },
      wind: {
        speed: 5.5,
      },
      sys: {
        sunrise: 102332423,
        sunset: 102332777,
      },
      weather: [
        { id: 1, main: 'cloudy', description: 'cloudy', icon: '02d' },
      ],
    } as CityWeather;

    fixture.detectChanges();
  });

  it('should render the component', () => {
    expect(fixture.nativeElement).toMatchSnapshot();
  });
});
