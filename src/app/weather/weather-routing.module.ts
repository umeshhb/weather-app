import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CityWeatherPageComponent } from './components/city-weather-page/city-weather-page.component';
import { WeatherForecastResolver } from './services/weather-forecast-resolver';

const routes: Routes = [
  {
    path: ':name/forecast',
    component: CityWeatherPageComponent,
    resolve: {
      forecast: WeatherForecastResolver,
    },
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule,
  ],
})
export class WeatherRoutingModule {}
