import { HttpHandler, HttpRequest } from '@angular/common/http';

import { HTTPInterceptor } from './http-interceptor';
import { TestBed } from '@angular/core/testing';

describe('HTTPInterceptor', () => {
  let service: HTTPInterceptor;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        HTTPInterceptor
      ],
    });

    service = TestBed.get(HTTPInterceptor);
  });

  it('should add an access token to the request', () => {
    const expectedResult = {};
    let handledReq!: HttpRequest<any>;

    const req = new HttpRequest('GET', 'some/url');
    const httpHandler = {
      handle: jest.fn(request => {
        handledReq = request;

        return expectedResult;
      }),
    } as unknown as HttpHandler;

    const result = service.intercept(req, httpHandler);

    expect(result).toBe(expectedResult);
  });
});
