import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';

const ERROR_MESSAGE = 'Unable to load data';
const UPDATE_ERROR_MESSAGE = 'Unable to update data';


@Injectable()
export class HTTPInterceptor implements HttpInterceptor {
  
  constructor(private readonly snackBar: MatSnackBar,
    private readonly router: Router
    ) {}

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
      const authenticatedReq = req.clone({
        params: req.params.set('APPID', '3d8b309701a13f65b660fa2c64cdc517'),
      }); 
      
      return next.handle(authenticatedReq).pipe(
        catchError(() => {
          const message = req.method === 'GET' ?
            ERROR_MESSAGE :
            UPDATE_ERROR_MESSAGE;
  
          this.showErrorNotification();
  
          return throwError(new Error(message));
        }),
      );
    }

  private showErrorNotification(): void {
    this.snackBar.open('An error occurred from server', 'CLOSE', {
      horizontalPosition: 'right',
      verticalPosition: 'top',
    });
  }
}
