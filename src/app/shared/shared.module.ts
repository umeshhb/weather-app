import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { ErrorMessageComponent } from './components/error-message/error-message.component';
import { ErrorGuardDirective } from './directives/error-guard/error-guard.directive';
import { MaterialDesignModule } from './material-design/material-design.module';

@NgModule({
  declarations: [
    ErrorGuardDirective,
    ErrorMessageComponent,
  ],
  entryComponents: [
    ErrorMessageComponent,
  ],
  exports: [
    BrowserAnimationsModule,
    MaterialDesignModule,
    HttpClientModule,
    ErrorGuardDirective,
    ErrorMessageComponent,
  ],
})
export class SharedModule {}
