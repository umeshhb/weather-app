import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LandingPageComponent } from './landing-page.component';

import { Component, Directive, Input, NO_ERRORS_SCHEMA } from '@angular/core';

import { By } from '@angular/platform-browser';
import { ErrorGuardDirective } from '../../shared/directives/error-guard/error-guard.directive';
import { WeatherService } from '../../weather/services/weather.service';
import { of } from 'rxjs';

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[routerLink]',
})
class RouterLinkMockDirective {
  @Input()
  routerLink!: string[];
}

@Component({
  selector: 'app-city-weather-card',
  template: '',
})
class CityWeatherCardMockComponent {}

describe('LandingPageComponent', () => {
  let fixture: ComponentFixture<LandingPageComponent>;

  let weatherServiceMock: WeatherService;
  const cityIds = [1, 2 ,3];
  const mockCitiesWeather = [
    { id: 1, name: 'City1' },
    { id: 2, name: 'City2' },
    { id: 3, name: 'City3' },
  ];

  beforeEach(() => {
    weatherServiceMock = {
      getWeatherByCityIds: jest.fn(() => of(mockCitiesWeather)),
    } as unknown as typeof weatherServiceMock;

    fixture = TestBed.configureTestingModule({
      declarations: [
        LandingPageComponent,
        RouterLinkMockDirective,
        CityWeatherCardMockComponent,
        ErrorGuardDirective,
      ],
      providers: [
        { provide: WeatherService, useValue: weatherServiceMock },
      ],
      schemas: [
        NO_ERRORS_SCHEMA,
      ],
    }).createComponent(LandingPageComponent);

    fixture.detectChanges();
  });

  it('should render the component', () => {
    expect(fixture.nativeElement).toMatchSnapshot();
  });

  it('should have routerLink initialized', () => {
    const cities = fixture.debugElement.queryAll(By.directive(CityWeatherCardMockComponent));

    expect(cities[0].injector.get(RouterLinkMockDirective).routerLink).toEqual(['/', 'City1', 'forecast']);
    expect(cities[1].injector.get(RouterLinkMockDirective).routerLink).toEqual(['/', 'City2', 'forecast']);
    expect(cities[2].injector.get(RouterLinkMockDirective).routerLink).toEqual(['/', 'City3', 'forecast']);
  });
});

