import { Component, OnInit } from '@angular/core';
import { of } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { CityWeather } from '../../weather/models/city-weather.model';
import { WeatherService } from '../../weather/services/weather.service';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.css']
})
export class LandingPageComponent implements OnInit {

  cityIds:any = [   
    2759794,
    2867714,
    2643743,
    2968815,
    3117735
  ];  
  
  citiesWeather$ = this.weatherService.fetchWeatherByIds(this.cityIds).pipe(
    catchError((e, _) => of(e)),
  );

  constructor(
    private readonly weatherService: WeatherService
  ) {  }

  trackById(index: number, value: CityWeather): number {
    return value.id;
  }

  ngOnInit() {
  }

}
